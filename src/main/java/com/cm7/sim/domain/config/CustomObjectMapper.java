package com.cm7.sim.domain.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsonorg.JsonOrgModule;
import org.bson.types.ObjectId;

public class CustomObjectMapper extends ObjectMapper {

    public CustomObjectMapper() {
        super();
        JsonOrgModule module = new JsonOrgModule();
        module.addSerializer(ObjectId.class, new ToStringSerializer());
        this.registerModule(module);
        this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        this.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, false);
        this.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);
    }
}
