package com.cm7.sim.domain.config;

import java.net.UnknownHostException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories("com.cm7.sim.domain.*")
public class MongoConfig {

    @Value("${mongodb.database}")
    String dbName;

    @Value("${mongodb.host}")
    String host;

    @Value("${mongodb.port}")
    Integer port;

    public String getDbName() {
        return dbName;
    }


    public void setDbName(String dbName) {
        this.dbName = dbName;
    }


    public String getHost() {
        return host;
    }


    public void setHost(String host) {
        this.host = host;
    }

    @Bean
    MongoTemplate mongoTemplate() throws UnknownHostException {
        var options = new MongoClientOptions.Builder()
            .cursorFinalizerEnabled(false)
            // .maxConnectionIdleTime(20000)
            // .connectionsPerHost(connectionPool)
            .threadsAllowedToBlockForConnectionMultiplier(1000)
            .connectTimeout(10000)
            .socketKeepAlive(true)
            .sslEnabled(true)
            .sslInvalidHostNameAllowed(true)
            .readPreference(ReadPreference.nearest())
            .writeConcern(WriteConcern.W1.withJournal(false))
            .socketFactory(getNoopSslSocketFactory())
            .build();

        var mongoClient = new MongoClient(new ServerAddress(this.host, this.port), options);
        MongoDbFactory m = new SimpleMongoDbFactory(mongoClient, dbName);

        var conv = new MappingMongoConverter(m, new MongoMappingContext());
        conv.setMapKeyDotReplacement("\\|");
        conv.setTypeMapper(new DefaultMongoTypeMapper(null));
        return new MongoTemplate(m, conv);
    }

    private static SSLContext getNoSSLContext() {
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, new TrustManager[] { new X509TrustManager() {

                @Override
                public void checkClientTrusted(X509Certificate[] x509Certificates, String s)
                    throws CertificateException {}

                @Override
                public void checkServerTrusted(X509Certificate[] x509Certificates, String s)
                    throws CertificateException {}

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            } }, new SecureRandom());

            return sslContext;
        }
        catch (Exception e) {
            return null;
        }
    }

    private static SSLSocketFactory getNoopSslSocketFactory() {
        try {
            return getNoSSLContext().getSocketFactory();
        }
        catch (Exception e) {
            return null;
        }
    }
}
