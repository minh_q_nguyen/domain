package com.cm7.sim.domain.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;
import com.lambdaworks.redis.ReadFrom;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisException;
import com.lambdaworks.redis.RedisURI;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.codec.ByteArrayCodec;
import com.lambdaworks.redis.masterslave.MasterSlave;
import com.lambdaworks.redis.masterslave.StatefulRedisMasterSlaveConnection;
import com.lambdaworks.redis.models.role.RedisNodeDescription;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.ExceptionTranslationStrategy;
import org.springframework.data.redis.PassThroughExceptionTranslationStrategy;
import org.springframework.data.redis.RedisConnectionFailureException;
import org.springframework.data.redis.connection.RedisClusterConnection;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisSentinelConnection;
import org.springframework.data.redis.connection.lettuce.LettuceConnection;
import org.springframework.data.redis.connection.lettuce.LettuceConverters;
import org.springframework.util.StringUtils;

public class MyLettuceConnectionFactory implements InitializingBean, DisposableBean, RedisConnectionFactory {

    private static final ExceptionTranslationStrategy EXCEPTION_TRANSLATION = new PassThroughExceptionTranslationStrategy(LettuceConverters.exceptionConverter());
    private String primaryHost;
    private String replicaHosts;
    private String sentinelHosts;
    private String masterID;
    private boolean isSentinel;
    private RedisClient client;
    private long timeout;
    private long shutdownTimeout;
    private boolean validateConnection;
    private boolean shareNativeConnection;
    private StatefulRedisConnection<byte[], byte[]> connection;
    private final Object connectionMonitor;
    private boolean convertPipelineAndTxResults;
    private boolean useSsl;
    private boolean verifyPeer;
    private boolean startTls;
    private Random rand = new Random();

    public MyLettuceConnectionFactory(String primaryHost, String masterID, String sentinelHosts, boolean isSentinel) {
        this.timeout = TimeUnit.MILLISECONDS.convert(60L, TimeUnit.SECONDS);
        this.shutdownTimeout = TimeUnit.MILLISECONDS.convert(2L, TimeUnit.SECONDS);
        this.validateConnection = false;
        this.shareNativeConnection = true;
        this.connectionMonitor = new Object();
        this.convertPipelineAndTxResults = false;
        this.useSsl = false;
        this.verifyPeer = true;
        this.startTls = false;
        this.primaryHost = primaryHost;
        this.sentinelHosts = sentinelHosts;
        this.masterID = masterID;
        this.isSentinel = isSentinel;
    }

    public void afterPropertiesSet() {
        this.client = this.createRedisClient();
    }

    public void destroy() {
        this.resetConnection();

        try {
            this.client.shutdown(this.shutdownTimeout, this.shutdownTimeout, TimeUnit.MILLISECONDS);
            if (this.connection != null) {
                this.connection.close();
            }
        } catch (Exception var3) {
            System.err.println("############# errror: " + var3.getMessage());
        }
    }

    public RedisConnection getConnection() {
        LettuceConnection connection = new LettuceConnection(this.getSharedConnection(), this.timeout, this.client, null, 0);
        connection.setConvertPipelineAndTxResults(this.convertPipelineAndTxResults);
        return connection;
    }

    public void initConnection() {
        synchronized(this.connectionMonitor) {
            if (this.connection != null) {
                this.resetConnection();
            }
            this.connection = this.createLettuceConnector();
        }
    }

    public void resetConnection() {
        synchronized(this.connectionMonitor) {
            if (this.connection != null) {
                this.connection.close();
            }
            this.connection = null;
        }
    }

    public void validateConnection() {
        synchronized(this.connectionMonitor) {
            boolean valid = false;
            if (this.connection.isOpen()) {
                try {
                    this.connection.sync().ping();
                    valid = true;
                } catch (Exception var5) {
                    System.err.println("############# errror: " + var5.getMessage());
                }
            }

            if (!valid) {
                System.err.println("############# Validation of shared connection failed. Creating a new connection");
                this.initConnection();
            }
        }
    }

    public DataAccessException translateExceptionIfPossible(RuntimeException ex) {
        return EXCEPTION_TRANSLATION.translate(ex);
    }

    public boolean getConvertPipelineAndTxResults() {
        return this.convertPipelineAndTxResults;
    }

    protected StatefulRedisConnection<byte[], byte[]> getSharedConnection() {
        if (this.shareNativeConnection) {
            synchronized(this.connectionMonitor) {
                if (this.connection == null) {
                    this.initConnection();
                }

                if (this.validateConnection) {
                    this.validateConnection();
                }

                return this.connection;
            }
        } else {
            return null;
        }
    }

    protected StatefulRedisConnection<byte[], byte[]> createLettuceConnector() {
        try {
            StatefulRedisMasterSlaveConnection<byte[], byte[]> connection;
            if (this.isSentinel){
                RedisURI lettuceURI = createRedisURISentinels(this.sentinelHosts, this.masterID);
                connection = MasterSlave.connect(this.client, new ByteArrayCodec(), lettuceURI);
                connection.setReadFrom(ReadFrom.MASTER_PREFERRED);
            }
            else {
                List<RedisURI> nodes = new ArrayList<>();
                RedisURI primaryURI =  createRedisURIAndApplySettings(primaryHost);
                nodes.add(primaryURI);

                if (StringUtils.hasText(replicaHosts)) {
                    Arrays.asList(this.replicaHosts.split(","))
                            .forEach(host -> {
                                RedisURI hostURI = RedisURI.create("redis://" + host);
                                hostURI.setSsl(this.useSsl);
                                hostURI.setVerifyPeer(this.verifyPeer);
                                hostURI.setStartTls(this.startTls);
                                hostURI.setUnit(TimeUnit.MILLISECONDS);
                                hostURI.setTimeout(this.timeout);
                                nodes.add(hostURI);
                            });
                }

                connection = MasterSlave.connect(this.client, new ByteArrayCodec(), nodes);

                connection.setReadFrom(new ReadFrom() {
                    @Override
                    public List<RedisNodeDescription> select(Nodes nodes) {
                        List<RedisNodeDescription> redisNodes = nodes.getNodes();
                        int ind = rand.nextInt(redisNodes.size());

                        List<RedisNodeDescription> result = new ArrayList<>();
                        result.add(redisNodes.get(ind));

                        IntStream.range(0, redisNodes.size())
                                .filter(j -> j != ind)
                                .mapToObj(redisNodes::get).forEach(result::add);

                        return result;
                    }
                });
            }
            return connection;
        } catch (RedisException var2) {
            throw new RedisConnectionFailureException("Unable to connect to Redis", var2);
        }
    }

    private RedisClient createRedisClient() {
        RedisURI uri;
        if (this.isSentinel){
            uri = this.createRedisURISentinels(this.sentinelHosts, this.masterID);
        }
        else {
            uri = this.createRedisURIAndApplySettings(this.primaryHost);
        }
        return RedisClient.create(uri);
    }

    private RedisURI createRedisURIAndApplySettings(String primaryHost) {
        RedisURI primaryURI =  RedisURI.create("redis://" + primaryHost);
        primaryURI.setSsl(this.useSsl);
        primaryURI.setVerifyPeer(this.verifyPeer);
        primaryURI.setStartTls(this.startTls);
        primaryURI.setUnit(TimeUnit.MILLISECONDS);
        primaryURI.setTimeout(this.timeout);
        return primaryURI;
    }

    private RedisURI createRedisURISentinels(String replicaHosts, String masterID) {
        return RedisURI.create("redis-sentinel://" + replicaHosts + "/0?sentinelMasterId=" + masterID + "&timeout=" + this.timeout +"ms");
    }

    @Override
    public RedisClusterConnection getClusterConnection() {
        return null;
    }

    @Override
    public RedisSentinelConnection getSentinelConnection() {
        return null;
    }

}
