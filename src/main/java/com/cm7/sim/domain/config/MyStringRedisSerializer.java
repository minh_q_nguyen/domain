package com.cm7.sim.domain.config;

import java.nio.charset.Charset;
import org.springframework.data.redis.serializer.StringRedisSerializer;

public class MyStringRedisSerializer extends StringRedisSerializer {

    private String namespaces;
    private final Charset charset;

    public MyStringRedisSerializer(String namespaces) {
        super();
        this.namespaces = namespaces;
        this.charset = Charset.forName("UTF8");
    }

    @Override
    public byte[] serialize(String string) {
        String key = new StringBuilder(namespaces).append(string).toString();
        return (string == null ? null : key.getBytes(charset));
    }
}
