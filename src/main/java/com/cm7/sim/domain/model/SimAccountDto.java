package com.cm7.sim.domain.model;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class SimAccountDto {

    @NotBlank
    String email;

    @NotBlank
    String username;

    @Length(min = 4)
    String password;

    public SimAccountDto() {}

    public SimAccountDto(String email, String username, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String setPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "SimAccountDto [email=" + email + ", password=" + password + ", username=" + username + "]";
    }

}
