package com.cm7.sim.domain.repository;

import java.util.Optional;

import com.cm7.sim.domain.repository.entity.AccountSession;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface AccountSessionRepository extends MongoRepository<AccountSession, String>, AccountSessionRepositoryExtend {

    @Query(value = "{'accountId': ?0}")
    Optional<AccountSession> findByAccountId(String accountId);

    @Query(value = "{'_id': ?0}", delete = true)
    void deleteByAccountId(String accountId);

}
