package com.cm7.sim.domain.repository;

public interface AccountSessionRepositoryExtend {

    void createNewSession(String accountId, String sessionId);
    void updateSession(String accountId, String sessionId);

}
