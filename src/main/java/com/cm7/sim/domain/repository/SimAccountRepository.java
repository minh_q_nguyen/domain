package com.cm7.sim.domain.repository;

import java.util.List;
import java.util.Optional;
import com.cm7.sim.domain.repository.entity.SimAccount;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface SimAccountRepository extends MongoRepository<SimAccount, String> {

    Optional<SimAccount> findByEmail(String email);

    @Query(value = "{'_id': {$in:?0}}", delete = true)
    void deleteAccountByIds(List<String> list);
}
