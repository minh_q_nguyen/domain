package com.cm7.sim.domain.repository.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "SimAccount")
public class SimAccount {

    @Field
    String id;

    @Field
    String email;

    @Field
    String password;

    @Field
    String username;

    @Field
    Long lastLogin;

    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public String getUsername() {
        return username;
    }


    public void setUsername(String username) {
        this.username = username;
    }


    public Long getLastLogin() {
        return lastLogin;
    }


    public void setLastLogin(Long lastLogin) {
        this.lastLogin = lastLogin;
    }


}
