package com.cm7.sim.domain.repository.impl;

import com.cm7.sim.domain.repository.AccountSessionRepositoryExtend;
import com.cm7.sim.domain.repository.entity.AccountSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

@Repository
public class AccountSessionRepositoryImpl implements AccountSessionRepositoryExtend {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void createNewSession(String accountId, String sessionId) {
        var ac = new AccountSession();
        ac.setSessionId(sessionId);
        ac.setAccountId(accountId);
        mongoTemplate.insert(ac, "AccountSession");
        System.out.println(String.format("created new session with account id '%s' and sessionId '%s'",
            accountId, sessionId));
    }

    @Override
    public void updateSession(String accountId, String sessionId) {
        Query query = new Query(Criteria.where("accountId").is(accountId));
        Update update = new Update();
        update.set("sessionId", sessionId);
        update.set("accountId", accountId);
        mongoTemplate.updateFirst(query, update, "AccountSession");
        System.out.println(
            String.format("update session with account id '%s' and sessionId '%s'", accountId, sessionId));
    }

}
