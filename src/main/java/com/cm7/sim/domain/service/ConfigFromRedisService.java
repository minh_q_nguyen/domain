package com.cm7.sim.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfigFromRedisService {

    private static final String TEST_KEY = "test_key";

    @Autowired
    RedisTemplateService redisTemplateService;

    public String getTestConfigString() {
        var testConfig = (String) redisTemplateService.get(TEST_KEY);
        if (testConfig == null) {
            testConfig = "this is value of test config";
            redisTemplateService.set(TEST_KEY, testConfig);
        }
        return testConfig;
    }

}
