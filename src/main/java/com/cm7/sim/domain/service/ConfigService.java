package com.cm7.sim.domain.service;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ConfigService {

    @Autowired
    ConfigFromRedisService configFromRedisService;

    @PostConstruct
    public void init() {
        var testKey = configFromRedisService.getTestConfigString();
        System.out.println("\u001B[33m testKey = '" + testKey + "' \u001B[0m");
    }

}
