package com.cm7.sim.domain.service;

import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisTemplateService {

    @Autowired
    RedisTemplate redisTemplate;

    public void set(String key, Object val) {
        if (key == null || val == null) {
            return;
        }
        redisTemplate.opsForValue().set(key, val, 30, TimeUnit.SECONDS);
    }

    public Object get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public void delete(String key) {
        redisTemplate.delete(key);
    }

    public void flush() {
        redisTemplate.getConnectionFactory().getConnection().flushAll();
    }
}
